import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContextExecutor, Future, Promise}

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

object WebServer {
  def main(args: Array[String]) {

    implicit val system: ActorSystem = ActorSystem("my-system")
    implicit val materializer: ActorMaterializer = ActorMaterializer()
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext: ExecutionContextExecutor = system.dispatcher

    val route: Route =
      path("") {
        get {
          complete("/")
        }
      } ~
        path("a") {
          get {
            complete("a")
          }
        }

    val f: Future[Done] = for {
      bindingFuture <- Http().bindAndHandle(route, "0.0.0.0", 8080)
      waitOnFuture <- Promise[Done].future
    }
      yield {
        waitOnFuture
      }

    println(s"Server online at http://0.0.0.0:8080/")
    Await.ready(f, Duration.Inf)
  }
}
