# Simple App

This application is a simple API that answers to both `/` and `/a`.

To make it work, please first build the project:

    sbt assembly

And then run the app:

    java -jar target/scala-2.12/simple-api-scala-assembly-1.0.0.jar

Test for jenkins hook.
