name := "simple-api-scala"

ThisBuild / organization := "io.dathena"
ThisBuild / version := "1.0.0"
ThisBuild / scalaVersion := "2.12.6"

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-explaintypes",
  "-unchecked",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Ywarn-unused:imports",
  "-Ywarn-unused:locals",
  "-Ywarn-unused:params",
  "-Ywarn-unused:privates",
  "-Ypartial-unification",
  "-encoding",
  "utf8"
)

lazy val dependencies = libraryDependencies ++= {
  val akkaVersion:     String = "2.5.19"
  val akkaHttpVersion: String = "10.1.7"
  val circeVersion:    String = "0.10.1"
  val scalikeJdbcV:    String = "3.3.1"

  Seq(
    // Akka
    "com.typesafe.akka" %% "akka-http"   % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion
  )
}

/*
lazy val mergeStrategy = assemblyMergeStrategy in assembly := {
  case m if m.toLowerCase.endsWith("manifest.mf")          => MergeStrategy.discard
  case m if m.toLowerCase.matches("meta-inf.*\\.sf$")      => MergeStrategy.discard
  case m if m.toLowerCase.startsWith("meta-inf/services/") => MergeStrategy.filterDistinctLines
  case m if m.toLowerCase.contains("license")              => MergeStrategy.concat
  case "reference.conf"                                    => MergeStrategy.concat
  case _                                                   => MergeStrategy.first
}*/

lazy val root = (project in file("."))
  .settings(dependencies)
  .settings(autoCompilerPlugins := true)
  .settings(assembly := (assembly dependsOn dependencyUpdates).value)


// No need to run tests while building jar
test in assembly := {}
// Simple and constant jar name
assemblyJarName in assembly := s"app-assembly.jar"
// Merge strategy for assembling conflicts
assemblyMergeStrategy in assembly := {
  case PathList("reference.conf") => MergeStrategy.concat
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
